import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'topics',
      component: () => import('@/views/TopicsView.vue'),
    },
    {
      path: '/last-viewed',
      name: 'last-viewed',
      component: () => import('@/views/TopicsView.vue'),
    },
    {
      path: '/post/:id',
      name: 'view-topic',
      component: () => import('@/views/TopicView.vue'),
    }
  ]
})

export default router
