import { transformAndValidate } from 'class-transformer-validator';
import { PostRequestValidator, PostResponseValidator } from '@/entities/post/validators';
import { getPosts, getPost, sendPost, sendTopic } from '@/shared/api';
import {
  mapFromToPost,
  mapToPost,
  mapToPosts,
  mapToResponse
} from '@/entities/post/mappers';
import type { IPostReq } from './model';

export const getPostsAndValidate = async () => {
  const response = await getPosts();
  const validated = await transformAndValidate(
    PostResponseValidator,
    response
  );
  
  return mapToPosts(validated);
}

export const getPostAndValidate = async (uuid: string) => {
  const response = await getPost(uuid);
  const validated = await transformAndValidate(
    PostResponseValidator,
    response
  );

  return mapToPost(validated);
}

export const sendNewPost = async (data: IPostReq) => {
  const validate = await transformAndValidate(
    PostRequestValidator,
    {
      ...data,
      ...(
        data.answer_id
          ? { answer_id: data.answer_id }
          : { answer_id: null }
      ),
    }
  );
  const mapped = mapToResponse(validate);

  const response = mapped.answer_id
    ? await sendPost(mapped.text, mapped.answer_id)
    : await sendTopic(mapped.text);

  return mapFromToPost({
    id: response,
    text: data.text,
    answerId: data.answer_id,
    createdAt: new Date(),
  });
}
