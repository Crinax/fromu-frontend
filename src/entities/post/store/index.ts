import { defineStore } from "pinia";
import type { IPost } from "@/entities/post/model";
import { getPostsAndValidate, sendNewPost } from "..";

export type PostStore = {
  data: IPost[],
}

export interface IPostStoreActions {
  receive(): Promise<void>
  send(text: string, answer_id?: string): Promise<void>
}

export type PostStoreGetters = {
  topics: (state: PostStore) => IPost[],
  comments: (state: PostStore) => IPost[],
  postMap: (state: PostStore) => Record<IPost['id'], IPost>,
  postCommentsMap: (state: PostStore) => Record<IPost['id'], IPost[]>
}

export const usePostStore = defineStore<
  'posts',
  PostStore,
  PostStoreGetters,
  IPostStoreActions
>('posts', {
  state: () => ({ data: [] }),
  getters: {
    topics: ({ data }) => data.filter(post => post.answerId === null),
    comments: ({ data }) => data.filter(post => post.answerId !== null),
    postMap: ({ data }) => data
      .map(post => ({ [post.id]: post }))
      .reduce((a, b) => ({ ...a, ...b }), {}),
    postCommentsMap: ({ data }) => data
      .map(post => ({ [post.id]: data.filter(p => p.answerId === post.id) }))
      .reduce((a, b) => ({ ...a, ...b }), {}),
  },
  actions: {
    async receive() {
      const posts = await getPostsAndValidate();
      const mapped = posts.map(item => item.get());

      this.data.splice(0, this.data.length);
      this.data.push(...mapped);
    },
    async send(text: string, answer_id?: string) {
      const new_post = await sendNewPost({
        text,
        answer_id
      });

      this.data.push(new_post.get());
    }
  },
})
