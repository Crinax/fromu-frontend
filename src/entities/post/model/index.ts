export interface IPost {
  id: string,
  text: string,
  answerId: string | null,
  createdAt: Date,
}

export interface IPostReq {
  text: string,
  answer_id: string,
}

export class Post {
  constructor(
    private id: string,
    private text: string,
    private answerId: string | null,
    private createdAt: Date,
  ) { }

  public isTopic(): boolean {
    return this.answerId === null
  }

  public get(): IPost {
    return {
      id: this.id,
      text: this.text,
      answerId: this.answerId,
      createdAt: this.createdAt,
    }
  }
}
