import { Post, type IPostReq } from '@/entities/post/model/index';
import type { PostResponseValidator, PostRequestValidator } from '@/entities/post/validators';

export function mapToPosts(data: PostResponseValidator[]): Post[] {
  return data.map(mapToPost);
}

export function mapToPost(data: PostResponseValidator): Post {
  return new Post(
    data.id,
    data.text,
    data.answer_id,
    new Date(Date.parse(data.created_at))
  );
}

export function mapToResponse(data: PostRequestValidator): IPostReq {
  if (data.answer_id) {
    return {
      text: data.text,
      answer_id: data.answer_id,
    };
  }

  return { text: data.text };
}

export function mapFromToPost(data: IMapFromData): Post {
  return new Post(
    data.id,
    data.text,
    data.answerId ?? null,
    data.createdAt,
  );
}

export interface IMapFromData {
  id: string,
  text: string,
  createdAt: Date,
  answerId?: string,
}
