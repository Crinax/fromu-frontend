import { IsUUID, IsString, ValidateIf } from 'class-validator'

export class PostResponseValidator {
  @IsUUID(4)
  id!: string

  @IsString()
  text!: string

  @ValidateIf((_, item) => item !== null)
  @IsString()
  answer_id!: string

  @IsString()
  created_at!: string
}

export class PostRequestValidator {
  @IsString()
  text!: string

  @ValidateIf((_, item) => item !== null)
  @IsString()
  answer_id!: string
}