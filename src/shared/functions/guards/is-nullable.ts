export function isNullable(value: unknown): value is undefined {
  return value === null || value === undefined;
}
