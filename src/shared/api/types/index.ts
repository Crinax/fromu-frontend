export type PostsResponse = {
  id: string,
  text: string,
  answer_id: string,
  created_at: string,
}

export type ErrorResponse = {
  error: true,
  message: string,  
}

export type TopicResponse = {
  id: string,
  text: string,
  answer_id: null,
  created_at: string,
}