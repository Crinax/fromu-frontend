import axios from 'axios';

export const apiAxios = axios.create({
  baseURL: import.meta.env.VITE_SERVER_HOST
})