import { apiAxios } from "./_axios-instance"
import type { ErrorResponse, PostsResponse, TopicResponse } from './types'

export const getPosts = async (): Promise<PostsResponse[]> => {
  return getRequest<PostsResponse[]>('/posts');
}

export const getPost = async (uuid: string): Promise<PostsResponse> => {
  return getRequest<PostsResponse>(`/posts/${uuid}`);
}

export const getTopics = async (): Promise<TopicResponse[]> => {
  return getRequest<TopicResponse[]>('/topics');
}

export const getTopic = async (uuid: string): Promise<TopicResponse> => {
  return getRequest<TopicResponse>(`/topics/${uuid}`);
}

export const sendPost =
  async (text: string, reply_to: string): Promise<string> =>
{
  return postRequest<string>('/posts', { text, reply_to });
}

export const sendTopic = async (text: string): Promise<string> =>
{
  return postRequest<string>('/topics', { text });
}

const getRequest = async <T>(url: string) => {
  const response = await apiAxios.get(url);

  return response.status === 200
    ? Promise.resolve(response.data as T)
    : Promise.reject(response.data as ErrorResponse)
}

const postRequest = async <T>(url: string, data: Record<string, any>) => {
  const response = await apiAxios.post(url, data);

  return response.status === 200
    ? Promise.resolve(response.data as T)
    : Promise.reject(response.data as ErrorResponse)
}